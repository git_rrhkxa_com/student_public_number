let path = require('path');

let http = require('http');

let express = require('express');

let bodyParser = require('body-parser');





let app = express();

app.use(require('morgan')('short'));
app.use('/demo',express.static(path.resolve(__dirname,'./src/')));

// app.use('/com',express.static(path.resolve(__dirname,'./common/')));
app.use(bodyParser.json()); // for parsing application/json
app.use(bodyParser.urlencoded({extended: true})); // for parsing application/x-www-form-urlencoded


// ************************************
// This is the real meat of the example
// ************************************


// Do anything you like with the rest of your express application.

app.get("/", function (req, res) {
    res.sendFile(__dirname + '/mUpload.html');
});


/*------------------测试接口------------------*/



/*------------------测试接口------------------*/

if (require.main === module) {
    let server = http.createServer(app);
    server.listen(process.env.PORT || 8030, function () {
        console.log("Listening on %j", server.address());
    });
}