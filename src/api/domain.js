/*!
 * domain.js v1.0.0
 * 域名配置类,生产模式和上线模式两种情况
 * 上线时候需要更改域名
 * Author:
 * Date: 2018-6-12
 * name:祝建新
 */


//切换生产模板
const whetherProduction = true;
const dataState = {
    student: 'stu',
    teacher: 'parent',
};

const student = (whetherProduction == true) ? 'http://api.xuchaohua.com/api/' + dataState.student + '/' : 'http://192.168.6.125:80/api/' + dataState.student + '/';
const parent = (whetherProduction == true) ? 'http://api.xuchaohua.com/api/' + dataState.teacher + '/' : 'http://192.168.6.125:80/api/' + dataState.teacher + '/';


export {student,parent};
