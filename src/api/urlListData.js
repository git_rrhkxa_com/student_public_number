/*!
 * urlListData.js v1.0.0
 * 接口地址
 * 需要写新接口的时候在这里面更改
 * Author:
 * Date: 2018-6-12
 * name:祝建新
 * student学生接口  teacher老师接口
 */

import {student,parent} from './domain.js';

//对应接口的api
const Api = {
    //<--学生接口S-->
    //考勤记录
    attendanceRecord : student + 'attendanceDetail',
    //个人资料
    personalData : student + 'basicInfo',
    //课程表
    classScheduleCard : student + 'course',
    //考试成绩
    examResults : student + 'exResult',
    //考试成绩图
    achievementMap : student + 'exStatistics',
    //学生首页
    indexStudent : student + 'index',
    //请假记录
    LeaveRecord : student + 'leaveByTime',
    //学生注册
    studentRegistration : student + 'register',
    //学分
    credit : student + 'scoreChange',
    //班级变更/寝室变更/物资领取
    classChange : student + 'studentChangeLog',
    //评价记录
    evaluationRecord : student + 'studentEvaluate',
    //<--学生接口E-->



    //<--老师接口S-->
    //attendances
    attendances : parent + 'attendanceDetail',
    //bind
    bind : parent + 'bind',
    //exResult
    exResult : parent + 'exResult',
    //exStatistics
    exStatistics : parent + 'exStatistics',
    //index
    index : parent + 'index',
    //scoreChange
    scoreChange : parent + 'scoreChange',
    //smsCode
    smsCode : parent + 'smsCode',
    //changeLog
    changeLog : parent + 'changeLog',
    //studentEvaluate
    studentEvaluate : parent + 'studentEvaluate',
    //<--老师接口E-->
};


export default {
    Api
};
export {
    Api
};
