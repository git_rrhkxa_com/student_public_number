/**
 * ajax封装
 * url 发送请求的地址
 * data 发送到服务器的数据，数组存储，如：{"username": "张三", "password": 123456}
 * succCallback 成功回调函数
 * errorCallback 失败回调函数
 * type 请求方式("POST" 或 "GET")， 默认已经设置为 "POST"
 * dataType 预期服务器返回的数据类型，常用的如：xml、html、json、text
 * reference jquery-1.7.1.js
 * name 祝建新
 */
//是否开启headers属性
const isOpen = false;





//登录验证需要在头部添加属性,需要从本地缓存取出来
const headerData = {
    Cookie:'',
    token:'',
};

let html = "";
html += '<div class="js_loading">';
html +=     '<div class="mask"></div>';
html +=     '<div class="loading">';
html +=         '<span><img src="../img/lodingOne.gif"></span>';
html +=     '</div>';
html += '</div>';
$("body").append(html);





//ajax二次配置后续需要任何配置在添加
const $ajax = function(pdata) {
    $.ajax({
        type: pdata.type,
        url: pdata.url,
        data: pdata.postData,
        dataType: pdata.dataType,
        headers: isOpen ? headerData : {},
        beforeSend: function(){  //开始loading
            $(".js_loading").show();
        },
        success: function (res) {
            if (res.code == 0) {
                if (pdata.succCallback) {
                    pdata.succCallback(res);
                }
            } else {
                if (pdata.errorCallback) {
                    pdata.errorCallback(res);
                }
            }
        },
        complete: function () {
            $(".js_loading").hide();
        }
    });
};

export default $ajax



